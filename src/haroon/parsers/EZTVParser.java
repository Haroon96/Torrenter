package haroon.parsers;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by M_har on 5/22/2017.
 */
public class EZTVParser extends Parser {
	public EZTVParser(String query, int resultCount) {
		super(query, resultCount);
	}
	
	@Override
	protected String makeUrl(String query) {
		return "https://eztv.ag/search/" + query.replace(' ', '-');
	}
	
	@Override
	public String getSiteName() {
		return "EZTV";
	}
	
	@Override
	protected Elements extractResults() throws Exception {
		Document doc = getDocument(query);
		Elements results = new Elements();
		Elements elements = doc.getElementsByTag("tr");
		int count = 0;
		for (Element e : elements) {
			if (e.className().equals("forum_header_border")) {
				results.add(e);
				++count;
			}
			if (count == resultCount) {
				break;
			}
		}
		return results;
	}
	
	@Override
	protected String extractMagnet(Element e) {
		return e.getElementsByClass("magnet").get(0).attr("href");
	}
	
	@Override
	protected String extractSeed(Element e) {
		return e.getElementsByTag("td").get(5).getElementsByTag("font").html();
	}
	
	@Override
	protected String extractSize(Element e) {
		return e.getElementsByTag("td").get(3).html();
	}
	
	@Override
	protected String extractTitle(Element e) {
		return e.getElementsByClass("epinfo").get(0).html();
	}
}

