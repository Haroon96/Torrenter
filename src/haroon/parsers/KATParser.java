package haroon.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by M_har on 5/22/2017.
 */
public class KATParser extends Parser {
	
	private int pageNo = 1;
	
	public KATParser(String query, int resultCount) {
		super(query, resultCount);
	}
	
	@Override
	protected String makeUrl(String query) {
		if (pageNo == 1) {
			return "https://kickass.bypassed.gold/search.php?q=" + query.replace(' ', '+');
		} else {
			return "https://kickass.bypassed.gold/usearch/" + query + "/" + pageNo + "/";
		}
	}
	
	@Override
	public String getSiteName() {
		return "KickAssTorrents";
	}
	
	@Override
	protected Elements extractResults() throws Exception {
		boolean resultsEnded = false;
		int count = 0;
		Elements results = new Elements();
		while (!resultsEnded) {
			Document doc = getDocument(query);
			Elements elements = doc.getElementsByTag("tr");
			int oldCount = count;
			for (Element e : elements) {
				if (e.className().equals("odd")) {
					results.add(e);
					++count;
				}
				if (count == resultCount) {
					resultsEnded = true;
					break;
				}
			}
			if (count == oldCount) {
				resultsEnded = true;
			}
			++pageNo;
		}
		return results;
	}
	
	@Override
	protected String extractMagnet(Element e) {
		return e.getElementsByClass("icon16").get(1).attr("href");
	}
	
	@Override
	protected String extractSeed(Element e) {
		return e.getElementsByClass("green").get(0).html();
	}
	
	@Override
	protected String extractSize(Element e) {
		return e.getElementsByClass("nobr").get(0).html().replace("&nbsp;", " ");
	}
	
	@Override
	protected String extractTitle(Element e) {
		return e.getElementsByClass("cellMainLink").get(0).html();
	}
	
}

