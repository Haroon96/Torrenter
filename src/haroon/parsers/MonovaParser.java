package haroon.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by M_har on 5/22/2017.
 */
public class MonovaParser extends Parser {
	
	private int pageNo = 1;
	
	public MonovaParser(String query, int resultCount) {
		super(query, resultCount);
	}
	
	@Override
	protected String makeUrl(String query) {
		return "https://monova.org/search?term=" + query + "&page=" + pageNo;
	}
	
	@Override
	public String getSiteName() {
		return "Monova";
	}
	
	@Override
	protected Elements extractResults() throws Exception {
		boolean resultsEnded = false;
		int count = 0;
		Elements results = new Elements();
		while (!resultsEnded) {
			Document doc = getDocument(query);
			Elements elements = doc.getElementsByClass("desktop");
			int oldCount = count;
			for (Element e : elements) {
				if (!e.classNames().contains("success")) {
					results.add(e);
					++count;
				}
				if (count >= resultCount) {
					resultsEnded = true;
					break;
				}
			}
			if (count == oldCount) {
				resultsEnded = true;
			}
			++pageNo;
		}
		return results;
	}
	
	@Override
	protected String extractMagnet(Element e) throws Exception {
		Document doc = Jsoup.connect("https:" + e.getElementsByTag("a").get(0).attr("href")).post();
		return doc.getElementById("download-file").attr("href");
	}
	
	@Override
	protected String extractSeed(Element e) {
		return "-";
	}
	
	@Override
	protected String extractSize(Element e) {
		return e.getElementsByTag("td").get(1).html();
	}
	
	@Override
	protected String extractTitle(Element e) {
		return e.getElementsByTag("a").get(0).html();
	}
	
}

