package haroon.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by M_har on 5/22/2017.
 */
public class TPBParser extends Parser {
	
	private int pageNo = 0;
	
	public TPBParser(String query, int resultCount) {
		super(query, resultCount);
	}
	
	@Override
	protected String makeUrl(String query) {
		return "https://thepiratebay.org/search/" + query + "/" + pageNo + "/";
	}
	
	@Override
	public String getSiteName() {
		return "ThePirateBay";
	}
	
	@Override
	protected Elements extractResults() throws Exception {
		boolean resultsEnded = false;
		Elements results = new Elements();
		while (!resultsEnded) {
			Document doc = getDocument(query);
			Element table = doc.getElementById("searchResult");
			Elements elements = table.getElementsByTag("tr");
			int oldCount = results.size();
			if (elements.size() > 0) {
				elements.remove(0);
			}
			results.addAll(elements);
			if (results.size() >= oldCount) {
				resultsEnded = true;
			}
			++pageNo;
		}
		return results;
	}
	
	@Override
	protected String extractMagnet(Element e) {
		return e.getElementsByTag("a").get(3).attr("href");
	}
	
	@Override
	protected String extractSeed(Element e) {
		return e.getElementsByTag("td").get(2).html();
	}
	
	@Override
	protected String extractSize(Element e) {
		String s = e.getElementsByClass("detDesc").get(0).html();
		return s.substring(s.indexOf("Size") + 5, s.indexOf("ULed by") - 2).replace("&nbsp;", "");
	}
	
	@Override
	protected String extractTitle(Element e) {
		return e.getElementsByClass("detLink").get(0).html();
	}
	
}

