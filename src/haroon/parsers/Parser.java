package haroon.parsers;

import haroon.holders.Torrent;
import jdk.nashorn.internal.runtime.JSONFunctions;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by M_har on 5/22/2017.
 */
public abstract class Parser {

	Parser(String query, int resultCount) {
		this.query = query;
		this.resultCount = resultCount;
	}
	
	protected abstract String makeUrl(String query);
	public abstract String getSiteName();
	
	public void parse(final TorrentFoundListener torrentFoundListener, int searchId) throws Exception {
		
		Elements entries = extractResults();
		
		String title, magnet, size, seeds;
		
		for (Element e : entries) {
			title = extractTitle(e);
			magnet = extractMagnet(e);
			size = extractSize(e);
			seeds = extractSeed(e);
			torrentFoundListener.torrentFound(new Torrent(title, magnet, getSiteName(), size, seeds), searchId);
		}
	}
	
	protected Document getDocument(String query) throws Exception {
		return Jsoup.connect(makeUrl(query)).post();
	}
	
	protected abstract Elements extractResults() throws Exception;
	protected abstract String extractTitle(Element e);
	protected abstract String extractMagnet(Element e) throws Exception;
	protected abstract String extractSize(Element e);
	protected abstract String extractSeed(Element e);
	
	protected String query = null;
	protected int resultCount;
	
	public interface TorrentFoundListener {
		void torrentFound(Torrent torrent, int searchId);
	}
}
