package haroon.holders;

/**
 * Created by M_har on 5/22/2017.
 */
public class Torrent {
	
	public Torrent(String title, String magnet, String site) {
		this(title, magnet, site, "?", "?");
	}
	
	public Torrent(String title, String magnet, String site, String size) {
		this(title, magnet, size, site,"?");
	}
	
	public Torrent(String title, String magnet, String site, String size, String seeds) {
		this.title = title;
		this.magnet = magnet;
		this.size = size;
		this.seeds = seeds;
		this.site = site;
	}
	
	public String hash() {
		return hash(title, size, seeds, site);
	}
	
	public static String hash(String title, String size, String seeds, String site) {
		return title + size + seeds + site;
	}
	
	private String title;
	private String magnet;
	private String size;
	private String seeds;
	private String site;
	
	public String getTitle() {
		return title;
	}
	
	public String getMagnet() {
		return magnet;
	}
	
	public String getSize() {
		return size;
	}
	
	public String getSeeds() {
		return seeds;
	}
	
	public String getSite() {
		return site;
	}
}
