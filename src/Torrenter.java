import haroon.holders.Torrent;
import haroon.parsers.*;
import uriSchemeHandler.URISchemeHandler;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by M_har on 5/22/2017.
 */
public class Torrenter {
	private JPanel panel1;
	private JTextField searchField;
	private JButton searchButton;
	private JTable searchResults;
	private JLabel status;
	private JSpinner countSpinner;
	private JButton about;
	private DefaultTableModel defaultTableModel;
	private Parser.TorrentFoundListener listener;
	private HashMap<String, String> magnets;
	private int searchId;
	
	private Torrenter() {
		this.magnets = new HashMap<>();
		listener = new Parser.TorrentFoundListener() {
			@Override
			public void torrentFound(Torrent torrent, int searchId) {
				if (searchId == Torrenter.this.searchId) {
					synchronized (magnets) {
						defaultTableModel.addRow(new Object[]{torrent.getTitle(), torrent.getSite(), torrent.getSize(), torrent.getSeeds()});
						magnets.put(torrent.hash(), torrent.getMagnet());
					}
				}
			}
		};
		searchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				updateTable();
			}
		});
		searchField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyChar() == '\n') {
					searchButton.getActionListeners()[0].actionPerformed(null);
				}
			}
		});
		SpinnerModel spinnerModel = new SpinnerNumberModel(20, 1, 100, 1);
		countSpinner.setModel(spinnerModel);
		defaultTableModel = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		defaultTableModel.setColumnIdentifiers(new String[]{"Title", "Site", "Size", "Seeds"});
		
		searchResults.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = searchResults.rowAtPoint(new Point(e.getX(), e.getY()));
				String title = searchResults.getValueAt(row, 0).toString();
				String site = searchResults.getValueAt(row, 1).toString();
				String size = searchResults.getValueAt(row, 2).toString();
				String seeds = searchResults.getValueAt(row, 3).toString();
				String magnetLink = magnets.get(Torrent.hash(title,size,seeds,site));
				try {
					URI magnetLinkUri = new URI(magnetLink);
					URISchemeHandler uriSchemeHandler = new URISchemeHandler();
					uriSchemeHandler.open(magnetLinkUri);
					
					String[] responses = new String[]{ "Copy to clipboard", "Close" };
					int n = JOptionPane.showOptionDialog(panel1, "If download failed, manually add the magnet " +
									"link to your torrent client", "Starting your torrent client...",
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
							null, responses, responses[0]);
					
					if (n == JOptionPane.YES_OPTION) {
						StringSelection stringSelection = new StringSelection(magnetLink);
						Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
						clpbrd.setContents(stringSelection, null);
					}
					
				} catch (Exception e0) {
					JOptionPane.showMessageDialog(panel1, "Something went wrong");
				}
			}
		});
		
		DefaultTableCellRenderer tableCellRenderer = new DefaultTableCellRenderer();
		tableCellRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		searchResults.setRowHeight(30);
		searchResults.setModel(defaultTableModel);
		searchResults.setAutoCreateRowSorter(true);
		searchResults.getColumn("Site").setCellRenderer(tableCellRenderer);
		searchResults.getColumn("Size").setCellRenderer(tableCellRenderer);
		searchResults.getColumn("Seeds").setCellRenderer(tableCellRenderer);
		setJTableColumnsWidth(searchResults, 500, 60, 20, 10, 10);
		about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(panel1, "Torrenter\n" +
						"Developed by Muhammad Haroon\nm_haroon96@hotmail.com\n\n" +
						"Supported sites: Monova, KickAssTorrents, EZTV, ThePirateBay\n\n" +
						"Source available at https://gitlab.com/Haroon23/Torrenter", "About", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
	
	private void updateTable() {
		while (defaultTableModel.getRowCount() != 0) {
			defaultTableModel.removeRow(0);
		}
		if (searchField.getText().equals("")) {
			JOptionPane.showMessageDialog(panel1, "Please enter a query");
			return;
		}
		int resultCount = new Integer(countSpinner.getValue().toString());
		String query = searchField.getText();
		Parser[] parsers = new Parser[]{
				new TPBParser(query, resultCount),
				new MonovaParser(query, resultCount),
				new KATParser(query, resultCount),
				new EZTVParser(query, resultCount)
		};
		status.setText("Searching for \"" + query + "\"");
		this.searchId = new Random().nextInt();
		new Thread(new Runnable() {
			@Override
			public void run() {
				ArrayList<Thread> threads = new ArrayList<>();
				for (Parser p : parsers) {
					Thread t = (new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								int tmp = searchId;
								p.parse(listener, searchId);
							} catch (Exception e) {
								JOptionPane.showMessageDialog(panel1, "Couldn't parse " + p.getSiteName());
							}
						}
					}));
					threads.add(t);
					t.start();
				}
				
				for (Thread t : threads) {
					try {
						t.join();
					} catch (Exception e) {}
				}
				
				if (defaultTableModel.getRowCount() > 0) {
					status.setText("Search finished. Click on a torrent to start downloading.");
				} else {
					status.setText("Search finished. No matches found.");
				}
			}
		}).start();
		
	}
	
	private void createUIComponents() {
		// TODO: place custom component creation code here
	}
	
	public static void setJTableColumnsWidth(JTable table, int tablePreferredWidth,
											 double... percentages) {
		double total = 0;
		for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
			total += percentages[i];
		}
		for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
			TableColumn column = table.getColumnModel().getColumn(i);
			column.setPreferredWidth((int)(tablePreferredWidth * (percentages[i] / total)));
		}
	}
	
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		
		}
		
		JFrame frame = new JFrame("Torrenter");
		frame.setMinimumSize(new Dimension(700, 500));
		frame.setContentPane(new Torrenter().panel1);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		
	}
}
